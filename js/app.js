(function( public ) {
	
	var btnMyTasks = document.querySelector('.my-tasks');

	btnMyTasks.addEventListener('click', function(ev){
		ev.preventDefault();
		React.render(<LifeApp />, document.querySelector('.app'));
	}, false);

	public.EVT = new EventEmitter2();

	public.originZone;

	public.tasks = {
					toDo: [
						{id:"1", name:"Car", color:"red", deadline:"2015-06-05", desc:"Bla Bla"},
						{id:"2", name:"Shopping", color:"violet", deadline:"2015-09-12", desc:"Do shopping"},
					],
					inProgress: [
						{id:"2", name:"Repairs", color:"blue", deadline:"2015-09-12", desc:"Make some repairs"},
					],
					finished:	[
						{id:"2", name:"Write the article", color:"green", deadline:"2015-03-12", desc:"Guy Trevor from Outside, 0045 458 782"},
					],
				};

	public.findAndDestroy = function(arr, element){
		var elPos = arr.map(function(x) {return x.name; }).indexOf(element.props.name);
		arr.splice(elPos, 1);
	}

})( this );
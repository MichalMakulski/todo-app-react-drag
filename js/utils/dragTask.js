function dragTask(e, el){
		if(e.button === 0){
			var _this = this;
			var dropZones = this.refs.manager.getDOMNode().children;
			var currentDropZone;
			var startX, startY;
			var htmlEl = el.getDOMNode();
			var rectEl = htmlEl.getBoundingClientRect();
			htmlEl.style.position = "relative";   
			htmlEl.classList.add('dragged');
			startX = rectEl.left;
	    startY = rectEl.top;
			document.onmousemove = function(e){
				htmlEl.style.left = e.clientX - startX - rectEl.width / 1.3 + "px";
		    htmlEl.style.top = e.clientY - startY - rectEl.height / 2 + "px";
				for(i=0; i<dropZones.length; i++){   
		      var rectDropzone = dropZones[i].getBoundingClientRect();   
		      var over = (e.clientX > rectDropzone.left) && (e.clientY > rectDropzone.top);
		      var leave = (e.clientX < rectDropzone.left + rectDropzone.width) && (e.clientY  < rectDropzone.top + rectDropzone.height);       
		      if( over && leave ){
		         dropZones[i].classList.add('active');    
		         currentDropZone = dropZones[i].id;   
		      }else {
		         dropZones[i].classList.remove('active');
		      }        
		     }
	    	}; 
			e.target.onmouseup = function(e){
						tasks[currentDropZone].push({
							name:el.props.name,
							color:el.props.color,
							deadline:el.props.deadline,
							desc:el.props.desc,
						});					
					findAndDestroy(tasks[originZone], el)	
					for(i=0; i<dropZones.length; i++){		
						dropZones[i].classList.remove('active');
					}
				_this.updateTasks();		
				htmlEl.style.position = "static";
	      htmlEl.style.left = "";
	      htmlEl.style.top = "";
				document.onmousemove = null;
				htmlEl.classList.remove('dragged');
			}
		}
	}
var TasksManager = React.createClass({

  render: function(){
    return (
    	<div className="row">
    		<ToDo toDoMediator = { this.props.toDo } />
    		<InProgress inProgressMediator = { this.props.inProgress } />
    		<Finished finishedMediator = { this.props.finished }/>
    	</div>
    );
  }
});

var Task = React.createClass({
	grab: function(e){
		EVT.emit('task-is-dragged', e, this);
	},
	render: function(){
		return (
			<li className="task" onMouseDown = { this.grab } style={{ "background" : this.props.color }}>
				<h3 className="task-name">{ this.props.name }</h3><br />
				<p className="task-deadline">Deadline:&nbsp; { this.props.deadline }</p><br />
				<p className="task-desc">{ this.props.desc }</p>
			</li>
		);
	}
});

var ToDo = React.createClass({	
	hello: function(e){
		originZone = this.getDOMNode().id;
	},
	render: function() {
	return (
		<div className="col-md-4 outer-tasks-container" id="toDo" onMouseDown = { this.hello }>
			<h2>To do</h2>
			<div className="inner-tasks-container">
				<ul>
					{this.props.toDoMediator.map(function(task, id){
			            return <Task key = { id } name = { task.name } color = { task.color } deadline = { task.deadline } desc = { task.desc }/>
			          })}
				</ul>
			</div>  
		</div>
	);
	}
});

var InProgress = React.createClass({
	hello: function(e){
		originZone = this.getDOMNode().id;
	},
  render: function() {
    return (
      <div className="col-md-4 outer-tasks-container" id="inProgress" onMouseDown = { this.hello }>
        <h2>In progress</h2>
        <div className="inner-tasks-container">
       		<ul>
				{this.props.inProgressMediator.map(function(task, id){
		            return <Task key = { id } name = { task.name } color = { task.color } deadline = { task.deadline } desc = { task.desc }/>
		          })}
			</ul>
        </div>  
      </div>
    );
  }
});

var Finished = React.createClass({
	hello: function(e){
		originZone = this.getDOMNode().id;
	},
  render: function() {
    return (
      <div className="col-md-4 outer-tasks-container" id="finished" onMouseDown = { this.hello }>
        <h2>Finished</h2>
        <div className="inner-tasks-container">
        <ul>
				{this.props.finishedMediator.map(function(task, id){
		            return <Task key = { id } name = { task.name } color = { task.color } deadline = { task.deadline } desc = { task.desc }/>
		          })}
			</ul>
        </div>  
      </div>
    );
  }
});
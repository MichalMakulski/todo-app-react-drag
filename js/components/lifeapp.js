var LifeApp = React.createClass({
	getInitialState: function(){
		return {
			toDoTasks: tasks.toDo,
			inProgressTasks: tasks.inProgress,
			finishedTasks: tasks.finished
		}
	},
	updateTasks: function(){
		this.setState({
			toDoTasks: tasks.toDo,
			inProgressTasks: tasks.inProgress,
			finishedTasks: tasks.finished
		});
	},
	drag: dragTask, 
	componentDidMount: function(){
		EVT.on('task-added', this.updateTasks);
		EVT.on('task-is-dragged', this.drag);
	},
  render: function(){
    return (
	    <div className="inner-container">
				<TaskForm />
			  <hr />
			  <TasksManager ref="manager" toDo = { this.state.toDoTasks } inProgress = { this.state.inProgressTasks } finished = { this.state.finishedTasks } />
			</div>   
		);
 	}
});

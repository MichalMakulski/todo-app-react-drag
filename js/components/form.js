var TaskForm = React.createClass({
	getInitialState: function(){
		return {
			name:"name",
			color:"color",
			deadline:"deadline",
			desc:"details",
		}
	},
	createTask: function(){
		tasks.toDo.push({
			name: this.refs.taskName.getDOMNode().value,
			color: this.refs.taskColor.getDOMNode().value,
			deadline: this.refs.taskDeadline.getDOMNode().value,
			desc: this.refs.taskDesc.getDOMNode().value,
		});
		EVT.emit('task-added');
	},
	render:function(){
		return(
			<div className="row">
				<div className="col-md-4">
					<div className="form-group">
						<TaskLabel forAttr = { "task" + this.state.name } text = { "Task " + this.state.name + ":"}/>
						<TaskNameInput ref="taskName"/>
						<TaskLabel forAttr = { "task" + this.state.color } text = { "Task " + this.state.color + ":"}/>
						<TaskColorInput ref="taskColor"/>
						<TaskLabel forAttr = { "task" + this.state.deadline } text = { "Task " + this.state.deadline + ":"}/>
						<TaskDeadLineInput ref="taskDeadline"/>
					</div>
				</div>  
				<div className="col-md-8">
					<div className="form-group">
						<div className="col-lg-10">
							<TaskLabel forAttr = { "task" + this.state.deadline } text = { "Task " + this.state.desc + ":"}/>
							<TaskDescInput ref="taskDesc" />
						</div>
					</div>
						<div className="col-lg-10">
							<TaskBtn addTask = { this.createTask } />
					</div>
				</div> 
			</div> 
		);
	}
});

var TaskLabel = React.createClass({
	render: function(){
		return (
			<label className="control-label" htmlFor={ this.props.forAttr }>{ this.props.text }</label>
		);
	}
});

var TaskNameInput = React.createClass({
  render: function(){
    return (
		<input className="form-control form-task" id="taskname" type="text" />
    );
  }
});

var TaskColorInput = React.createClass({
  render: function(){
    return (
        <input className="form-control form-task" id="taskcolor" type="color" />
    );
  }
});

var TaskDeadLineInput = React.createClass({
  render: function(){
    return (
        <input className="form-control form-task" id="taskdeadline" type="date" placeholder="rrrr-mm-dd" />
    );	   
  }
});

var TaskDescInput = React.createClass({
  render: function(){
    return (
        <textarea className="form-control form-task" rows="3" id="taskdetails"></textarea>
    );
  }
});

var TaskBtn = React.createClass({
  render: function(){
    return <button type="submit" id="addTaskBtn" onClick = { this.props.addTask } className="btn btn-primary">Add new task</button>
  }
});
